PassThePopcorn Plex Plug-in
===========================

Installation
------------

Download the archive, and extract the PassThePopcorn.bundle folder to your plugin directory.

Settings
--------

Change the values for your username, password, and passkey in order to login and enable all the menu items.
You passkey is the key the tracker uses to connect your torrents and your account. In order to get your passkey, go to `https://tls.passthepopcorn.me/upload.php`. Right below the "Do not upload" table, there is a text box with "Your personal announce URL is:" above it. In that text box should be a link which looks like `http://please.passthepopcorn.me:2710/random6numbers6and6letters/announce`. For our example, our passkey would be `random6numbers6and6letters`. It would be very easy to make a mistake typing this out, so it may be easier to copy and paste this through the web interface for your first setup.

By default, the plugin will only download one image for each rip. Changing it to show the background image as well may slow down image loading significantly, especially for HD rips.

Bugs
----
*  A movie listing on the web interface shows only the year instead of the title
*  "Latest Torrents" page is unreliable
*  The GP icon isn't rendered nicely
*  Search pop up doesn't display unless the prompt starts with "Search" (a "feature" of Plex)
*  Windows paths require backslash escaping (e.g. C:\\\\Users\\\\kannibalox\\\\Watch)

To do
-----
*  Add more rip information
    *  Have I snatched/uploaded/etc
    *  Text that is not mediainfo or markup
    *  Edition titles
    *  Basic mediainfo stuff (audio channels, langs, subs, etc)
    *  Trumpable
*  More pretty artwork
*  Handle all html entities properly
*  Streamline and improve xpath queries if possible
*  Find a proper way to enable the different view options, rather than the fake movie hack